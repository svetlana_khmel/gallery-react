# React gallery component with settings. #

* React- * Redux- * Webpack- *Express

Gallery screenshot:
![Scheme](http://res.cloudinary.com/dmt6v2tzo/image/upload/v1503419869/Screen_Shot_2017-08-22_at_7.35.35_PM_ddnjcs.png)

#Goal:
1. To build project from the scratch using Git, npm, babel, Webpack, Express, React;

2. To develop gallery component.

** Find it on Heroku: **

https://gallery-component-example.herokuapp.com/


* Should be different types of animation (opacity, transition)
* Should be switch on / switch off autoplay
* Autoplay should play if 'on' in settings and  on the start of user has not clicked yet on bullet
* Settings => 'Show bullets | Show arrow navigation' | Show titles | Show description
* Settings => 'Show in 1 column | Show in 2 columns' | Responsive | Set width of element
* Active bullet is selected when arrow navigation

** Study how to:
   * compile css from sass
   * copy static images | fonts to build directory with webpack
   * use animation with react

---

Setup:

To build project from the scratch I have used this tutorial:
 - http://andrewhfarmer.com/build-your-own-starter

 **** npm ***

To install or update nvm, you can use the install script using cURL:

curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.0/install.sh | bash

The script clones the nvm repository to ~/.nvm and adds the source line to your profile (~/.bash_profile, ~/.zshrc, ~/.profile, or ~/.bashrc).

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh" # This loads nvm

------

 In this step we add a package.json file to record our npm dependencies.

 Everything we add in subsequent steps will be an npm package: Babel, Webpack, React, etc.

 Creating the package.json file is as simple as running the npm init command shown below.

 We also add a .gitignore file in this step. That file only has one line: node_modules. This tells git to ignore the node_modules folder. We don't need to store it in git because it can be auto-generated.

 Notice that the README has changed. Since this is now an npm project, the npm install command can be used to install all dependencies. That's worth noting in the README.

 Directions
 1. Run these commands
 npm init -y
 2. Add these files
 .gitignore
 3. Update these files
 README.md
 4. Then run these commands
 git add .
 git commit -m "add npm package.json"


==========>>>>> files <<<<============

==>>>>> .gitignore :

 + node_modules

==>>>>> package.json :

+ {
    "name": "my-project",
    "version": "1.0.0",
    "description": "My Project",
    "main": "index.js",
    "scripts": {
      "test": "echo \"Error: no test specified\" && exit 1"
    },
    "keywords": [],
    "author": "",
    "license": "ISC"
  }

==>>>>> readme.md :

  My Project
  ---

  Welcome.



  Setup
  ---

  ```
  npm install
  ```



*** Babel ***

Babel transforms the ES6 JavaScript that we will be writing into ES5 JavaScript that current browsers can understand.

Babel is particularly important for React developers because of JSX. JSX is a succinct way to write out the layout of a React component.

To use Babel we need to install Babel itself, as well as two additional packages for configuring Babel for use with ES6 and React.

The .babelrc file configures Babel to use the presets that we just installed.

Directions
1. Run these commands
npm install --save babel-core
npm install --save babel-preset-es2015
npm install --save babel-preset-react
2. Add these files
.babelrc
3. Then run these commands
git add .
git commit -m "add babel"


==========>>>>> files <<<<============

==>>>>> .babelrc :

+ {
+    "presets": ["es2015", "react"]
+ }

==>>>>> package.json :

+ {
    "author": "",
+    "dependencies": {
+      "babel-core": "^6.9.1",
+      "babel-preset-es2015": "^6.9.0",
+      "babel-preset-react": "^6.5.0"
    },
    "description": "My Project",
    "keywords": [],
    "license": "ISC",
    "main": "index.js",
    "name": "my-project",
    "scripts": {
      "test": "echo \"Error: no test specified\" && exit 1"
    },
    "version": "1.0.0"
  }

*** Webpack ***

   Webpack bundles all your JavaScript together into a single file. This includes each JavaScript file that you write as well as your npm packages.

   Why Bundle?
   A single .js file is easier to deploy and will usually download faster than multiple small files.

   Webpack will work with Babel to convert your code from ES6 to ES5 while we work. Webpack can also minify your .js file for production.

   Webpack Config
   The main addition in this step is the webpack.config.js file.

   You will set six configuration options in this file. Here is the breakdown for each one, along with a link to the relevant portion of the Webpack docs:

   context: This is the path to your client-side JavaScript folder. This must be an absolute path.

   entry: This is the entry point for your application.

   module.loaders: This section specifies how each file should be processed before it is combined into your bundle. We only have one loader: Babel. This converts your ES6 + JSX JavaScript into ES5 before merging it into your bundle.

   resolveLoader: Where Webpack should look for loaders, like the Babel loader mentioned above.

   resolve: Where Webpack should look for files referenced by an import or require() statement. This makes it so that you can import npm packages in your code.

   Directions
   1. Run these commands
   npm install --save webpack babel-loader
   2. Add these files
   src/main.js, webpack.config.js
   3. Then run these commands
   git add .
   git commit -m "add webpack"
   NOTE: babel-loader is a Webpack "loader". It supports running Babel from Webpack. Usually any npm package ending in -loader is for Webpack.

==========>>>>> files <<<<============

==>>>>> src/main.js :

==>>>>> package.json :

{
  "author": "",
  "dependencies": {
    "babel-core": "^6.9.1",
+    "babel-loader": "^6.2.4",
    "babel-preset-es2015": "^6.9.0",
    "babel-preset-react": "^6.5.0",
+    "webpack": "^1.13.1"
  },
  "description": "My Project",
  "keywords": [],
  "license": "ISC",
  "main": "index.js",
  "name": "my-project",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "version": "1.0.0"
}

==>>>>> webpack.config.js :

+ var path = require('path');

  var config = {
    context: path.join(__dirname, 'src'),
    entry: [
      './main.js',
    ],
    output: {
      path: path.join(__dirname, 'www'),
      filename: 'bundle.js',
    },
    module: {
      loaders: [
        {
          test: /\.js$/,
          exclude: /node_modules/,
          loaders: ['babel'],
        },
      ],
    },
    resolveLoader: {
      root: [
        path.join(__dirname, 'node_modules'),
      ],
    },
    resolve: {
      root: [
        path.join(__dirname, 'node_modules'),
      ],
    },
  };
  module.exports = config;

**** Compile ****

     In the last step you added Webpack, but you didn't actually get to use it yet.

     In this step you'll add an npm script called compile to your package.json. The script runs Webpack.

     Directions
     1. Update these files
     .gitignore, package.json, README.md
     Try it Out
     Run npm run compile to test out your new command. It should create a file called www/bundle.js. Open up that file and take a look. You'll see your main.js 'Hello World' code in there, along with a lot of Webpack boilerplate.

     In practice, you wouldn't run npm run compile very often. This step is about knowing exactly what Webpack does so that things do not seem muddled when we add more pieces to the puzzle.

     Now remove the www/bundle.js file. We won't need it in future steps.

==========>>>>> files <<<<============

==>>>>> .gitignore :

   node_modules
 + www/bundle.js

==>>>>> package.json :


{
  "author": "",
  "dependencies": {
    "babel-core": "^6.9.1",
    "babel-loader": "^6.2.4",
    "babel-preset-es2015": "^6.9.0",
    "babel-preset-react": "^6.5.0",
    "webpack": "^1.13.1"
  },
  "description": "My Project",
  "keywords": [],
  "license": "ISC",
  "main": "index.js",
  "name": "my-project",
  "scripts": {
+    "compile": "webpack",
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "version": "1.0.0"
}

*** Express ***

    A React development environment needs some way to show your app in a browser. That means you'll need a server. In this step you'll be setting up Express as your server.

    The main file added here is server.js. That file will run an Express server, which will run Webpack. Webpack will re-run any time you change one of your JavaScript files.

    server.js is server-side JavaScript. You can run it with the node server.js command. In this step you will also add a start script to run the server.

    You will create www/index.html as your first web page. Express is configured to serve all the files in that folder.

    Directions
    1. Run these commands
    npm install --save express webpack-dev-middleware
    2. Add these files
    server.js, www/index.html
    3. Update these files
    package.json, README.md
    Try the Server
    Before we go on, be sure to run your server by running the npm start command and then opening http://localhost:3000.

    You should see Hello World on the screen and Hello World in the developer console.

    Click "Next" to move on to the last and final step: React.

==========>>>>> files <<<<============

==>>>>> www/index.html :

+ <html>
  <head>
    <script src="/bundle.js" ></script>
  </head>
  <body>
    Hello World
  </body>
  </html>

==>>>>> package.json :
{
  "author": "",
  "dependencies": {
    "babel-core": "^6.9.1",
    "babel-loader": "^6.2.4",
    "babel-preset-es2015": "^6.9.0",
    "babel-preset-react": "^6.5.0",
+    "express": "^4.13.4",
    "webpack": "^1.13.1",
+    "webpack-dev-middleware": "^1.6.1"
  },
  "description": "My Project",
  "keywords": [],
  "license": "ISC",
  "main": "index.js",
  "name": "my-project",
  "scripts": {
    "compile": "webpack",
+    "start": "node server.js",
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "version": "1.0.0"
}


==>>>>> server.js :

  +  var express = require('express');
    var webpackDevMiddleware = require('webpack-dev-middleware');
    var webpack = require('webpack');
    var webpackConfig = require('./webpack.config.js');
    var app = express();

    var compiler = webpack(webpackConfig);

    app.use(express.static(__dirname + '/www'));

    app.use(webpackDevMiddleware(compiler, {
      hot: true,
      filename: 'bundle.js',
      publicPath: '/',
      stats: {
        colors: true,
      },
      historyApiFallback: true,
    }));

    var server = app.listen(3000, function() {
      var host = server.address().address;
      var port = server.address().port;
      console.log('Example app listening at http://%s:%s', host, port);
    });

*** React

    In this step you'll add the React npm package and one simple component.

    Directions
    1. Run these commands
    npm install --save react react-dom
    2. Add these files
    src/Counter.js
    3. Update these files
    src/main.js, www/index.html
    4. Then run these commands
    git add .
    git commit -m "add react"
    If you still have your server running, just refresh http://localhost:3000 and you should see your counter button. Click it to see the counter increment.

==========>>>>> files <<<<============

==>>>>> www/Counter.js :

    + import React from 'react';

      /**
       * A counter button: tap the button to increase the count.
       */
      class Counter extends React.Component {
        constructor() {
          super();
          this.state = {
            count: 0,
          };
        }

        render() {
          return (
            <button
              onClick={() => {
                this.setState({ count: this.state.count + 1 });
              }}
            >
              Count: {this.state.count}
            </button>
          );
        }
      }
      export default Counter;

==>>>>> www/main.js :

+    console.log('Hello World!');
    import React from 'react';
    import ReactDOM from 'react-dom';
    import Counter from './Counter';

    document.addEventListener('DOMContentLoaded', function() {
      ReactDOM.render(
        React.createElement(Counter),
        document.getElementById('mount')
      );
    });

==>>>>> www/index.html :


    <html>
    <head>
      <script src="/bundle.js" ></script>
    </head>
    <body>
      Hello World
+      <div id="mount"></div>
    </body>
    </html>


==>>>>> package.json :

{
  "author": "",
  "dependencies": {
    "babel-core": "^6.9.1",
    "babel-loader": "^6.2.4",
    "babel-preset-es2015": "^6.9.0",
    "babel-preset-react": "^6.5.0",
    "express": "^4.13.4",
 +   "react": "^15.1.0",
 +   "react-dom": "^15.1.0",
    "webpack": "^1.13.1",
    "webpack-dev-middleware": "^1.6.1"
  },
  "description": "My Project",
  "keywords": [],
  "license": "ISC",
  "main": "index.js",
  "name": "my-project",
  "scripts": {
    "compile": "webpack",
    "start": "node server.js",
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "version": "1.0.0"
}


======== Another modules =======

*** copy-webpack-plugin

    I tried multiple webpack loaders like the webpack file loader but I could not get a simple copy to work. (with the file loader I managed to get the PNG image moved but the image got corrupted)

    After more digging around I cam across the copy-webpack-plugin so I gave it a go.

    And Bingo! I got the job done.

    It's basically a very basic plugin to copy static file/folders to your build destination. Here is how you would configure it in your webpack config file.



==========>>>>> files <<<<============

==>>>>> webpack.config.js :
var path = require('path');

+ var CopyWebpackPlagin = require('copy-webpack-plugin');

var config = {
    context: path.join(__dirname, 'src'),
    entry: [
        './main.js',
        './styles/scss/main.scss'
    ],
    output: {
        path: path.join(__dirname, 'www'),
        filename: 'bundle.js',
    },
    module: {
        loaders: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loaders: ['babel'],
            },

            {
                test: /\.scss$/,
                loaders: ["style-loader", "css-loader", "sass-loader"]
            },

            { test: /\.(html)$/,
                loader: "file?name=[path][name].[ext]&context=./app/static"
            }
        ],
    },
  +  plugins: [
  +      new CopyWebpackPlagin([
  +          { from: './assets', to: './assets'}
  +      ])
  +  ],

    sassLoader: {
        includePaths: [path.resolve(__dirname, "./src/styles/scss/")]
    },
    resolveLoader: {
        root: [
            path.join(__dirname, 'node_modules'),
        ],
    },
    resolve: {
        root: [
            path.join(__dirname, 'node_modules'),
        ],
    }
};
module.exports = config;



---

To install the Project:

```
npm install
```
To Compile:
---

```
npm run compile
```
To Run: npm run