import data from '../data/data';

export function loadGalleryData (data) {
    return {
        type: 'DATA_LOADED',
        payload: data
    }
}

export function changeSettings (data) {
    console.log("CHANGE_SETTINGS action..", data);
    return {
        type: 'CHANGE_SETTINGS',
        payload: data
    }
}

export function fetchData() {
    return function (dispatch, getState) {
        return data.getData()
                .then((response) => response.json())
        .then((data) => dispatch(loadGalleryData(data)))
        .catch((err) => console.log(err));
    };
}