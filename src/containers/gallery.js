import React, { Component} from 'react';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as dataActions from "../actions/loadData";

import Arrows from "../components/arrows";
import Autoplay from "../components/autoplay";

class Gallery extends Component {
    constructor (props) {
        super();

        this.state = {
            newSettings: {},
            currentImage: 0,
            imagesInFrame: 0,
            transition: '',
            childTransition: '',
            resetAnimationStyle: '',
            intervalId: '',
            switchAutoplay: '',
            responsiveListItem: '',
            resize: '',
            numberOfFrames : ''
        };

        this.renderBullets = this.renderBullets.bind(this);
        this.navigateTo = this.navigateTo.bind(this);
        this.gotoNext = this.gotoNext.bind(this);
        this.gotoPrevious = this.gotoPrevious.bind(this);
        this.switchAutoplay = this.switchAutoplay.bind(this);
        this.autoplay = this.autoplay.bind(this);
        this.resize = this.resize.bind(this);
        this.transitionend = this.transitionend.bind(this);
        this.beforeTransitionStart = this.beforeTransitionStart.bind(this);
        this.setStyleToElements = this.setStyleToElements.bind(this);
    }

    componentWillMount () {
        this.setState({
            newSettings: this.props.newSettings
        });

        this.loadData();
    }

    componentDidMount () {

        if (this.props.newSettings.autoplay === 'on') {
            this.autoplay('start');
        }

        if (this.props.newSettings.responsive == 'on') {
            this.resize();
        }
    }

    transitionend () {

    }

    beforeTransitionStart() {
        this.setState({
            transition: 'none',
            childTransition: 'none'
        });

        setTimeout(() => { this.onTransitionStart(); }, 100);
    }

    setStyleToElements () {
        if (this.props.newSettings.responsive == 'on') {
            var element = this.refs['root-resize-container'];

            if(this.props.newSettings.responsive == 'on' && this.props.newSettings.animationType == 'slider'){
                this.state.resetContainerWidth = element.offsetWidth * 3;
                this.state.responsiveListItem = element.offsetWidth;
            }

            if(this.props.newSettings.responsive == 'on' && this.props.newSettings.animationType == 'vanish'){
                this.state.resetContainerWidth = element.offsetWidth;
                this.state.responsiveListItem = element.offsetWidth;
            }

        }

        if (this.props.newSettings.responsive == 'off') {

            if(this.props.newSettings.animationType == 'slider'){
                this.state.resetContainerWidth = this.props.newSettings.width * 3;
                this.state.responsiveListItem = this.props.newSettings.width;
            }

            if(this.props.newSettings.animationType == 'vanish'){
                this.state.resetContainerWidth = this.props.newSettings.width;
                this.state.responsiveListItem = this.props.newSettings.width;
            }
        }

        this.regularStyle = {
            width: this.state.resetContainerWidth
        };

        this.setState({
            resize: 'resize'
        });
    }

    onTransitionStart () {

        this.setState({
            transition: this.props.newSettings.animationType === 'slider' ? 'enter' : '',
            childTransition: this.props.newSettings.animationType === 'vanish'? 'vanish' : ''
        });

        this.setStyleToElements();

        var element = this.refs['gallery-list'];
        element.addEventListener("transitionend", this.transitionend, false);
    }

    gotoNext (event) {

        if ( event ) {
            this.autoplay ('stop');
        }

        this.setState({
            currentImage: (this.state.currentImage < this.state.data.length-1 ? this.state.currentImage + 1 : 0),
            resetAnimationStyle: ''
        });

        this.beforeTransitionStart();
    }

    gotoPrevious () {

        if ( event ) {
            this.autoplay ('stop');
        }

        this.setState({
            currentImage: (this.state.currentImage >= 1 ? this.state.currentImage - 1 : this.state.data.length - 1),
            resetAnimationStyle: ''
        });

        this.beforeTransitionStart ();
    }

    navigateTo (i, event) {

        if ( event ) {
            this.autoplay ('stop');
        }

        this.setState({
            currentImage: i,
            resetAnimationStyle: ''
        });

        this.beforeTransitionStart ();
    }

    switchAutoplay () {

        if (this.state.switchAutoplay === 'on') {
            this.setState({switchAutoplay: 'off'});
            this.autoplay('stop');
        } else {
            this.setState({switchAutoplay: 'on'});
            this.autoplay('start');
        }
    }

    autoplay (status) {

        if (status === 'start') {
            console.log("Autoplay has been started ", );

            var playInterval = window.setInterval(this.gotoNext, this.props.newSettings.autoplayInterval);

            this.setState({
                switchAutoplay: 'on',
                intervalId: playInterval
            });
        }

        if (status === 'stop') {
            clearInterval(this.state.intervalId);
            console.log("Autoplay has been stoped ", this.state.intervatId);

            this.setState({
                switchAutoplay: 'off'
            });
        }
    }

    resize() {
        console.log('... Window has been resized.');

        this.setStyleToElements();
    }

    renderList() {

        for (var index = 0; index <= this.state.data.length; index++) {

            if (index === this.state.currentImage) {

                var prevIndex = index-1;
                var nextIndex = index+1;

                return (

                    <ul ref="gallery-list" onLoad={()=> this.state.transition = ''} className={(this.props.newSettings.animationType === 'vanish' ? 'vanish-transition ': '') + "gallery-list " + this.state.transition} style = {this.regularStyle}>
                        {!this.state.data[prevIndex] &&
                        <li
                            // onClick={() => this.props.selectBook(book)}
                            key={this.state.data[this.state.numberOfFrames].title}
                            className="list-group-item gallery-item first-frame" style={ {backgroundImage: "url("+ this.state.data[this.state.numberOfFrames].imgUrl +")", width: this.state.responsiveListItem} }>
                            <h1>{this.state.data[this.state.numberOfFrames].title}</h1>
                            <div className="description">{this.state.data[this.state.numberOfFrames].description}</div>
                        </li>
                        }

                        {this.state.data[prevIndex] &&
                        <li
                            // onClick={() => this.props.selectBook(book)}
                            key={this.state.data[prevIndex].title}
                            className="list-group-item gallery-item first-frame" style={  {backgroundImage: "url(" + this.state.data[prevIndex].imgUrl + ")", width: this.state.responsiveListItem} }>
                            <h1>{this.state.data[prevIndex].title}</h1>
                            <div className="description">{this.state.data[prevIndex].description}</div>
                        </li>
                        }

                        <li
                            // onClick={() => this.props.selectBook(book)}
                            key={this.state.data[index].title}
                            className={(this.state.currentImage === 0 && this.state.childTransition === '' ? ' vanish ' : ' ') + "list-group-item gallery-item " + this.state.childTransition} style={ {backgroundImage: "url(" + this.state.data[index].imgUrl + ")",  width: this.state.responsiveListItem} }>
                            <h1>{this.state.data[index].title}</h1>
                            <div className="description">{this.state.data[index].description}</div>
                        </li>

                        {this.state.data[nextIndex] && this.props.newSettings.animationType === 'slider' &&
                        <li
                            // onClick={() => this.props.selectBook(book)}
                            key={this.state.data[nextIndex].title}
                            className="list-group-item gallery-item"
                            style={  {backgroundImage: "url(" + this.state.data[nextIndex].imgUrl + ")", width: this.state.responsiveListItem} }>
                            <h1>{this.state.data[nextIndex].title}</h1>
                            <div className="description">{this.state.data[nextIndex].description}</div>
                        </li>
                        }
                    </ul>
                );
            }
        }
    }

    renderBullets() {
        if (this.props.newSettings.bulletNavigation === 'on') {
            return this.state.galleryData.map((frame, index) => {
                return (
                    <div key={frame.title} className={(index === this.state.currentImage) ? "active" : "inactive" }  onClick={this.navigateTo.bind(this, index)}>{index + 1}</div>
                )
            })
        }
    }

    loadData () {

        this.props.fetchData().then(() => {
debugger;
            this.setState({
                data: this.props.data,
                numberOfFrames: this.props.data.length - 1
            });
        });
    }

    renderBullets() {
        if (this.state.data && this.props.newSettings.bulletNavigation === 'on') {

            return this.state.data.map((frame, index) => {
                return (
                    <div key={frame.title} className={(index === this.state.currentImage) ? "active" : "inactive" }  onClick={this.navigateTo.bind(this, index)}>{index + 1}</div>
                )
            })
        }
    }

    render() {

        return (
            <div ref="root-resize-container">

                {this.props.newSettings.autoplay === 'on' &&
                    <Autoplay switch={this.state.switchAutoplay} action={this.switchAutoplay} />
                }
                <div className="gallery-container" style={{width: this.state.responsiveListItem}}>

                    {this.state.data &&
                        this.renderList()
                    }

                    <div className="bullets-navigation">
                        {this.renderBullets()}
                    </div>

                    {this.props.newSettings.arrowNavigation === 'on' &&
                        <Arrows prev={this.gotoPrevious} next={this.gotoNext} />
                    }
                </div>

            </ div>
        )
    }
}

// export default connect(
//     state => ({
//     data: state
// }),
// dispatch => bindActionCreators(dataActions, dispatch)
// )(Gallery);


function mapStateToProps (state) {
    console.log('mapStateToProps.. Gallery.. ', state);

    return {
        newSettings: state.newSettings,
        data: state.galleryData.data
    }
}
//
// function mapDispatchToProps(dispatch) {
//     return {
//         getData: bindActionCreators(getData, dispatch)
//     }
// }

export default connect(mapStateToProps, dispatch => bindActionCreators(dataActions, dispatch))(Gallery)