import React, { Component } from 'react';
import Admin from './admin';
import Gallery from './gallery';

export default class App extends Component {

    render () {
        return (
            <div>
                <Gallery />
                <Admin />
            </div>
        )
    }
}
