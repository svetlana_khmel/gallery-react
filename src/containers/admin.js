import React, { Component} from 'react';

import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as dataActions from "../actions/loadData";

class Admin extends Component {
    constructor (props) {
        super();

        this.state = {
            announce: '',
            announceShown: ''
        };

        this.setResponsive = this.setResponsive.bind(this);
        this.setWidth = this.setWidth.bind(this);
        this.setHeight = this.setHeight.bind(this);
        this.setAnimation = this.setAnimation.bind(this);
        this.setAutoplay = this.setAutoplay.bind(this);
        this.setArrowNavigation = this.setArrowNavigation.bind(this);
        this.setBulletNavigation = this.setBulletNavigation.bind(this);
        this.setNewSettings = this.setNewSettings.bind(this);
        this.reset = this.reset.bind(this);
    }

    setNewSettings(property, value) {

        let settings = this.props.settings;
        settings[property] = value;

        this.props.changeSettings(settings);

        this.setState({
            announce : 'New settings has been applied',
            announceShown: 'shown'
        });

        window.setTimeout(()=>{
            this.setState({
                announce : '',
                announceShown: ''
            });
        }, 1000)
    }

    reset () {
        localStorage.clear();
        this.props.loadSettings();

        this.setState({
            announce : 'Settings has been reset',
            announceShown: 'shown'
        });

        window.setTimeout(()=>{
            this.setState({
                announce : '',
                announceShown: ''
            });
        }, 1000)
    }

    setResponsive (event) {
        this.setNewSettings('responsive', event.target.value);
    }

    setWidth (event) {
        this.setNewSettings('width', parseInt(event.target.value));
    }

    setHeight (event) {
        this.setNewSettings('height', event.target.value);
    }

    setAnimation (event) {
        this.setNewSettings('animationType', event.target.value);
    }

    setAutoplay (event) {
        this.setNewSettings('autoplay', event.target.value);
    }

    setAutoplayInterval (event) {
        this.setNewSettings('autoplayInterval', event.target.value);
    }

    setArrowNavigation (event) {
        this.setNewSettings('arrowNavigation', event.target.value);
    }

    setBulletNavigation (event) {
        this.setNewSettings('bulletNavigation', event.target.value);
    }

    render () {
        return (
            <div>

                <h1>Gallery settings: </h1>
                <div className={'announce ' + this.state.announceShown}>{this.state.announce}</div>

                <form>
                    <fieldset>
                        <legend>Width:</legend>
                        <div className="cell-settings">
                            <div>
                                <input type="radio" name="width-responsive" value="on" checked={this.props.settings.responsive === 'on'} onChange={this.setResponsive}/>
                                <label>Responsive</label>
                            </div>
                            <div>
                                {this.props.settings.responsive !== 'off' &&
                                <input type="radio" name="width-responsive" value="off" checked={this.props.settings.responsive === 'off'} onChange={this.setResponsive}/>
                                }
                                <label>Set width</label>
                            </div>

                            {this.props.settings.responsive === 'off' &&
                            <span>
                                    <input type="text" maxLength="4" value={this.props.settings.width} onChange={this.setWidth} onKeyUp={this.setWidth} required="
This field can't be empty"/>
                                    <label> px</label>
                                {this.props.settings.width === '' &&
                                    <div className="required">This field can't be empty</div>
                                }
                                </span>
                            }
                        </div>
                    </fieldset>

                    <fieldset>
                        <legend>
                            Animation:
                        </legend>

                        <div className="cell-settings">
                            <input type="radio" name="animation" value="slider" checked={this.props.settings.animationType === 'slider'} onChange={this.setAnimation}/>
                            <label> Slider </label>
                            <input type="radio" name="animation" value="vanish" checked={this.props.settings.animationType === 'vanish'} onChange={this.setAnimation}/>
                            <label> Vanish </label>
                        </div>
                    </fieldset>

                    <fieldset>
                        <legend>
                            Autoplay:
                        </legend>
                        <div className="cell-settings">
                            <input type="radio" name="autoplay" value="on" checked={this.props.settings.autoplay === 'on'} onChange={this.setAutoplay}/>
                            <label> Show </label>
                            <input type="radio" name="autoplay" value="off" checked={this.props.settings.autoplay === 'off'} onChange={this.setAutoplay}/>
                            <label> Hide </label>
                        </div>
                    </fieldset>

                    <fieldset>
                        <legend>
                            Autoplay interval:
                        </legend>
                        <div className="cell-settings">
                            <input type="text" maxLength="4" name="autoplay-interval" value={this.props.settings.autoplayInterval} onChange={this.setAutoplayInterval} />
                            <label> ms</label>
                        </div>
                    </fieldset>

                    <fieldset>
                        <legend>
                            Arrow navigation:
                        </legend>
                        <div className="cell-settings">
                            <input type="radio" name="arrows" value="on" checked={this.props.settings.arrowNavigation === 'on'}  onChange={this.setArrowNavigation}/>
                            <label> Show </label>
                            <input type="radio" name="arrows" value="off" checked={this.props.settings.arrowNavigation === 'off'}  onChange={this.setArrowNavigation}/>
                            <label> Hide </label>
                        </div>
                    </fieldset>

                    <fieldset>
                        <legend>
                            Bullet navigation:
                        </legend>
                        <div className="cell-settings">
                            <input type="radio" name="bullets" value="on" checked={this.props.settings.bulletNavigation === 'on'} onChange={this.setBulletNavigation}/>
                            <label> Show </label>
                            <input type="radio" name="bullets" value="off" checked={this.props.settings.bulletNavigation === 'off'} onChange={this.setBulletNavigation}/>
                            <label> Hide </label>
                        </div>
                    </fieldset>

                   {/* <input type="button" className="btn reset" value="Reset" onClick={this.reset}/>*/}
                </form>
            </div>
        );
    }
}

// export default connect(
//     state => ({
//     data: state
// }),
// dispatch => bindActionCreators(dataActions, dispatch)
// )(Admin);


function mapStateToProps (state) {
    console.log('mapStateToProps.. Admin.. ', state);

    return {
        settings: state.newSettings
    }
}

// function mapDispatchToProps(dispatch) {
//     return {
//         changeSettings: bindActionCreators(changeSettings, dispatch)
//     }
// }

export default connect(mapStateToProps, dispatch => bindActionCreators(dataActions, dispatch))(Admin)