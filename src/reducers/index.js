import { combineReducers } from 'redux';
import ReducerChangedSettings from './reducer_changed_settings';
import galleryData from './reducer_gallery_data';

const rootReducer = combineReducers({
    newSettings: ReducerChangedSettings,
    galleryData: galleryData
});

export default rootReducer;
