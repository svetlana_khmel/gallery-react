const initialState = {
    height: '',
    width: '',
    responsive: 'on',
    animationType: 'slider',
    autoplay: 'on',
    autoplayInterval: 3500,
    arrowNavigation: 'on',
    bulletNavigation: 'on'
};

export default function (state = initialState, action) {
    switch (action.type) {
        case 'CHANGE_SETTINGS':
            return {
                ...state,
                payload: action.payload
            };

            break;
    }

    return state;
}