import React, { Component} from 'react';

const Arrows = (props) => {
    return (
        <div className="autoplay-container">
            <div className={"autoplay-indicator " +  props.switch} onClick={props.action}>Autoplay</div>
        </div>
    )
};

export default Arrows;