import React, { Component} from 'react';

const Arrows = (props) => {
    return (
        <div className="arrow-navigation">
            <div className="goto-prev" onClick={props.prev}> &lt; </div>
            <div className="goto-next" onClick={props.next}> &gt; </div>
        </div>
    )
};

export default Arrows;